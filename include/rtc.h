/*
 * eeprom.h
 *
 *  Created on: 02 февр. 2015 г.
 *      Author: user5
 */

#ifndef RTC_H_
#define RTC_H_


#include "common.h"

void RTCInit();

#ifdef __cplusplus
extern "C" {
#endif

void RTC_Alarm_IRQHandler(void);

/*   DMA1_Stream6_IRQHandler */
void EEP_I2C_DMA_TX_IRQHandler(void);

/*   DMA1_Stream0_IRQHandler*/
void EEP_I2C_DMA_RX_IRQHandler(void);

#ifdef __cplusplus
}
#endif





#endif /* RTC_H_ */
