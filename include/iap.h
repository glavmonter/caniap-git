/*
 * iap.h
 *
 *  Created on: 01 марта 2015 г.
 *      Author: margel
 */

#ifndef IAP_H_
#define IAP_H_


#include <stm32f4xx.h>
#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>
#include <stdio.h>
#include "common.h"
#include "caniap.pb.h"

#define	IAP_VERSION			"0.0.1"

//#define CAN_OK 				(0)
#define CAN_TIMEOUT_ERROR	(-1)
#define CAN_ACK_ERROR		(-2)
#define CAN_MEMORY_ERROR	(-3)
#define CAN_SEMPTH_ERROR	(-4)
#define CAN_PB_ERROR		(-5)

uint32_t StartIap();

typedef enum {
	CAN_OK = 0,
	CAN_PKT_READY,
	CAN_INDEX_TO_LARGE,
	CAN_ERROR_PB,
	CAN_ERROR
} CANError;

typedef enum {
	CAN_STOPED,
	CAN_WAIT_HTD,
	CAN_RECEIVING
} CANState;

#define IAP_CMD1_ERASE      	(1)
#define IAP_CMD1_WRITE      	(2)
#define IAP_CMD1_START      	(3)
#define IAP_CMD1_CRC        	(4)
#define IAP_CMD1_ERASE_EEPROM	(5)

class IAP : public TaskBase {
public:
	IAP(uint8_t DeviceId, char const *name, unsigned portBASE_TYPE priority,
			uint16_t stackDepth = configMINIMAL_STACK_SIZE);

	void task();
	static void task_iap(void *param) {
		static_cast<IAP *>(param)->task();
		while (1)
			vTaskDelay(portMAX_DELAY);
	}

public:
	QueueHandle_t xCanQueue;

private:
	void InitCan();
	CANError ReceiveFromHost(CanRxMsg *RxMessage);
	CANError ProcessPkt();

	uint32_t m_iBytesToReceive;
	uint32_t m_iMessagesToReceive;
	uint32_t m_iMaxMessageIndex;
	uint32_t m_iCrcIn;
	uint32_t flashdst;

	uint32_t num_messages;
	uint32_t received_messages;
	uint32_t crc;

	uint8_t DeviceID;

	uint8_t m_vRxBuffer[HostToDevice_size];
	HostToDevice m_xHtD;
};


#ifdef __cplusplus
extern "C" {
#endif

void CAN1_RX0_IRQHandler(void);

#ifdef __cplusplus
}
#endif


#endif /* IAP_H_ */
