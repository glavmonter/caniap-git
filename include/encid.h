/*
 * encid.h
 *
 *  Created on: 18.02.2013
 *      Author: Vladimir Meshkov <glavmonter@gmail.com>
 */

#ifndef ENCID_H_
#define ENCID_H_

void ENCInit();
uint8_t ENCGetID();


#endif /* ENCID_H_ */
