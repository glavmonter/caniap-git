#ifndef CANIAP_INCLUDE_COMMON_H_
#define CANIAP_INCLUDE_COMMON_H_

#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include <new>
#include "stm32f4xx_conf.h"

class TaskBase {
public:
	TaskHandle_t handle;
	virtual ~TaskBase() {
#if INCLUDE_vTaskDelete
		vTaskDelete(handle);
#endif
		return;
	}
};


template <char GPIOx, uint16_t GPIOPin, uint16_t debounce> class Button {
public:
	Button():start(0), state(STATE_RESETED) {
		switch (GPIOx) {
		case 'A':
			gpiox = GPIOA;
			break;
		case 'B':
			gpiox = GPIOB;
			break;
		case 'C':
			gpiox = GPIOC;
			break;
		case 'D':
			gpiox = GPIOD;
			break;
		case 'E':
			gpiox = GPIOE;
			break;
		default:
			gpiox = NULL;
		}

		if (gpiox == NULL)
			return;

	GPIO_InitTypeDef GPIOInitStructure;
		GPIOInitStructure.GPIO_Pin = GPIOPin;
		GPIOInitStructure.GPIO_Mode = GPIO_Mode_IN;
		GPIOInitStructure.GPIO_PuPd = GPIO_PuPd_UP;
		GPIOInitStructure.GPIO_Speed = GPIO_Speed_25MHz;
		GPIO_Init(gpiox, &GPIOInitStructure);
	}


	bool Process(TickType_t currentTick) {
		if (gpiox == NULL)
			return false;

		uint8_t pinstate;
		pinstate = GPIO_ReadInputDataBit(gpiox, GPIOPin);

		switch (state) {
		case STATE_RESETED:
			state = STATE_WAIT_PRESSED;
			break;

		case STATE_WAIT_PRESSED:
			if (pinstate == Bit_RESET) {
				/* Нажатие */
				state = STATE_PRESSED;
				start = currentTick;
			}
			break;

		case STATE_PRESSED:
			if (pinstate == Bit_SET) {
				// Released
				state = STATE_WAIT_PRESSED;
			} else {
				if (currentTick - start > debounce) {
					state = STATE_WAIT_RELEASED;
					return true;
				}
			}
			break;

		case STATE_WAIT_RELEASED:
			if (pinstate == Bit_SET) {
				state = STATE_WAIT_PRESSED;
			}
			break;
		}

		return false;
	}

private:
	GPIO_TypeDef *gpiox;
	TickType_t start;
	enum {
		STATE_RESETED,
		STATE_WAIT_PRESSED,
		STATE_PRESSED,
		STATE_WAIT_RELEASED
	} state;
};

char *FloatToString(char *outstr, double value, int places, int minwidth = 0, bool rightjustify = false);

#ifdef __cplusplus
extern "C" {
#endif

void vApplicationTickHook();

#ifdef __cplusplus
}
#endif

#endif /* CANIAP_INCLUDE_COMMON_H_ */
