/*
 * eeprom.cpp
 *
 *  Created on: 02 февр. 2015 г.
 *      Author: user5
 */


#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include <timers.h>
#include <arm_math.h>
#include <core_cm4.h>
#include <rtc.h>

#include "hardware.h"
#include "diag/Trace.h"

static portTASK_FUNCTION_PROTO(RTCTask, pvParameters);
static portTASK_FUNCTION_PROTO(ResetTask, pvParameters);

static SemaphoreHandle_t xRTCSemaphore = NULL;


void RTCInit() {
RTC_InitTypeDef RTC_InitStructure;
RTC_AlarmTypeDef RTC_AlarmStructure;
EXTI_InitTypeDef EXTI_InitStructure;
NVIC_InitTypeDef NVIC_InitStructure;

	RCC_LSEConfig(RCC_LSE_ON);
	while (RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET) {
	}

	RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);
	RCC_RTCCLKCmd(ENABLE);
	RTC_WaitForSynchro();

	RTC_InitStructure.RTC_AsynchPrediv = 0x7F;
	RTC_InitStructure.RTC_SynchPrediv = 0xFF;
	RTC_InitStructure.RTC_HourFormat = RTC_HourFormat_24;
	RTC_Init(&RTC_InitStructure);

	/* Set the alarm 00h:01min:15s */
	RTC_AlarmStructure.RTC_AlarmTime.RTC_H12     = RTC_H12_AM;
	RTC_AlarmStructure.RTC_AlarmTime.RTC_Hours   = 0x00;
	RTC_AlarmStructure.RTC_AlarmTime.RTC_Minutes = 0x01;
	RTC_AlarmStructure.RTC_AlarmTime.RTC_Seconds = 0x15;
	RTC_AlarmStructure.RTC_AlarmDateWeekDay = 1;
	RTC_AlarmStructure.RTC_AlarmDateWeekDaySel = RTC_AlarmDateWeekDaySel_Date;
	RTC_AlarmStructure.RTC_AlarmMask = RTC_AlarmMask_DateWeekDay | RTC_AlarmMask_Hours | RTC_AlarmMask_Minutes; // Прерывание по будильнику, каждый час в 1:15

	RTC_AlarmCmd(RTC_Alarm_A, DISABLE);
	RTC_SetAlarm(RTC_Format_BCD, RTC_Alarm_A, &RTC_AlarmStructure);

	RTC_ITConfig(RTC_IT_ALRA, ENABLE);
	RTC_AlarmCmd(RTC_Alarm_A, ENABLE);
	RTC_ClearFlag(RTC_FLAG_ALRAF);

	/* EXTI configuration *********************************************************/
	EXTI_ClearITPendingBit(EXTI_Line17);
	EXTI_InitStructure.EXTI_Line = EXTI_Line17;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	/* Enable the RTC Alarm Interrupt, самый низший приоритет */
	NVIC_InitStructure.NVIC_IRQChannel = RTC_Alarm_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = configLIBRARY_LOWEST_INTERRUPT_PRIORITY;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);


	xRTCSemaphore = xSemaphoreCreateBinary();
	xSemaphoreTake(xRTCSemaphore, 0);

	xTaskCreate(RTCTask, "RTC", configMINIMAL_STACK_SIZE*1, NULL, tskIDLE_PRIORITY, NULL);
	xTaskCreate(ResetTask, "Reset", configMINIMAL_STACK_SIZE*1, NULL, tskIDLE_PRIORITY+1, NULL);
}


void vTimerCallback( TimerHandle_t pxTimer ) {
(void) pxTimer;
	NVIC_SystemReset();
}

static portTASK_FUNCTION(ResetTask, pvParameters) {
(void)pvParameters;

Button <'E', GPIO_Pin_9,  100> button1;
Button <'E', GPIO_Pin_13, 100> button2;
Button <'C', GPIO_Pin_6,  100> button3;
Button <'C', GPIO_Pin_8,  100> button4;

TimerHandle_t xTimer = xTimerCreate("Reset", 5000, pdFALSE, (void *)0, vTimerCallback);

	uart_printf("-----IAP-----\n");
	for (;;) {
		TickType_t CurrentTick = xTaskGetTickCount();
		if (button1.Process(CurrentTick)) {
			uart_printf("IAP: Button_1, Reset to Main: %d\n", CurrentTick);
			RTC_WriteBackupRegister(RTC_BKP_DR0, 0);
			xTimerStart(xTimer, 0);
		}
		if (button2.Process(CurrentTick)) {
			uart_printf("IAP: Button_2: %d\n", CurrentTick);
		}
		if (button3.Process(CurrentTick)) {
			uart_printf("IAP: Button_3: %d\n", CurrentTick);
		}
		if (button4.Process(CurrentTick)) {
			uart_printf("IAP: Button_4: %d\n", CurrentTick);
		}

		vTaskDelay(10);
	}
}

static portTASK_FUNCTION(RTCTask, pvParameters) {
(void)pvParameters;

RTC_TimeTypeDef RTC_TimeStructure;
RTC_AlarmTypeDef RTC_AlarmStructure;

	for (;;) {
		if (xSemaphoreTake(xRTCSemaphore, 1000) == pdTRUE) {
			RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
			RTC_GetAlarm(RTC_Format_BIN, RTC_Alarm_A, &RTC_AlarmStructure);

			uart_printf("IAP: Alarm - %0.2d:%0.2d:%0.2d\n\n", RTC_TimeStructure.RTC_Hours,
							RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
		}
	}
}

/**
  * @brief  This function handles RTC Alarms interrupt request.
  * @param  None
  * @retval None
  */
void RTC_Alarm_IRQHandler(void)
{
static BaseType_t xHigherPriorityTaskWoken;

	if(RTC_GetITStatus(RTC_IT_ALRA) != RESET) {
		RTC_ClearITPendingBit(RTC_IT_ALRA);
	    EXTI_ClearITPendingBit(EXTI_Line17);

	    xSemaphoreGiveFromISR(xRTCSemaphore, &xHigherPriorityTaskWoken);
	    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
	}
}
