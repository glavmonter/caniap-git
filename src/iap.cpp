/*
 * iap.cpp
 *
 *  Created on: 01 марта 2015 г.
 *      Author: margel
 */

#include <stm32f4xx.h>
#include <FreeRTOS.h>
#include <task.h>
#include <stdio.h>
#include <string.h>
#include <pb.h>
#include <pb_encode.h>
#include <pb_decode.h>

#include "hardware.h"
#include "flash_if.h"
#include "iap.h"
#include "rtc.h"
#include "encid.h"
#include "canmsg.h"
#include "caniap.pb.h"
#include "diag/Trace.h"

static IAP *iap = NULL;

static portTASK_FUNCTION_PROTO(vBlink, pvParameters);

uint32_t StartIap() {
	xTaskCreate(vBlink, "Blink", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY, NULL);
	ENCInit();

	iap = new IAP(ENCGetID(), "IAP", configMAX_PRIORITIES - 1, configMINIMAL_STACK_SIZE*3);

	vTaskStartScheduler();

	return 0;
}

static portTASK_FUNCTION(vBlink, pvParameters) {
(void)pvParameters;
	for (;;) {
		IWDG_ReloadCounter();
		LED4_PORT->ODR ^= LED4_PIN;
		vTaskDelay(100);
	}
}

IAP::IAP(uint8_t DeviceId, char const *name, unsigned portBASE_TYPE priority, uint16_t stackDepth) :
	flashdst(APPLICATION_ADDRESS) {

	this->DeviceID = DeviceId;
	xCanQueue = xQueueCreate(4, sizeof(CanRxMsg));
	FLASH_If_Init();

	InitCan();
	xTaskCreate(task_iap, name, stackDepth, this, priority, &handle);
}

void IAP::InitCan() {
GPIO_InitTypeDef GPIO_InitStructure;
NVIC_InitTypeDef NVIC_InitStructure;

	/* CAN1 config */
	GPIO_PinAFConfig(CAN_RX_GPIO_PORT, CAN_RX_SOURCE, CAN_RX_AF);
	GPIO_PinAFConfig(CAN_TX_GPIO_PORT, CAN_TX_SOURCE, CAN_TX_AF);

	GPIO_InitStructure.GPIO_Pin = CAN_TX_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(CAN_TX_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = CAN_RX_PIN;
	GPIO_Init(CAN_RX_GPIO_PORT, &GPIO_InitStructure);

	CAN_CLK_INIT(CAN_CLK, ENABLE);

	CAN_DeInit(CANLIB_CAN);

CAN_InitTypeDef CAN_InitStructure;
	CAN_InitStructure.CAN_TTCM = DISABLE;
	CAN_InitStructure.CAN_ABOM = DISABLE;
	CAN_InitStructure.CAN_AWUM = DISABLE;
	CAN_InitStructure.CAN_NART = DISABLE;
	CAN_InitStructure.CAN_RFLM = DISABLE;
	CAN_InitStructure.CAN_TXFP = DISABLE;
	CAN_InitStructure.CAN_Mode = CAN_Mode_Normal;
	CAN_InitStructure.CAN_SJW = CAN_SJW_1tq;

	/* 1 MBps ?? */
	CAN_InitStructure.CAN_BS1 = CAN_BS1_14tq;
	CAN_InitStructure.CAN_BS2 = CAN_BS2_6tq;
	CAN_InitStructure.CAN_Prescaler=4;        //  500 kbit/s
	CAN_Init(CANLIB_CAN, &CAN_InitStructure);

	/* TODO Configure filters */
CAN_FilterInitTypeDef CAN_FilterInitStructure;
	CAN_FilterInitStructure.CAN_FilterNumber = 0;
	CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdList;
	CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_16bit;
	CAN_FilterInitStructure.CAN_FilterIdHigh = GENID(DeviceID, CANMSG_ACK) << 5; // 0x91
	CAN_FilterInitStructure.CAN_FilterIdLow = GENID(DeviceID, CANMSG_NACK) << 5; // 0xA1
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh = GENID(DeviceID, CANMSG_DTH) << 5; // 0xC1
	CAN_FilterInitStructure.CAN_FilterMaskIdLow = GENID(DeviceID, CANMSG_HTD) << 5; // 0xB1
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment = CAN_Filter_FIFO0;
	CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
	CAN_FilterInit(&CAN_FilterInitStructure);

	CAN_FilterInitStructure.CAN_FilterNumber = 1;
	CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdList;
	CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_16bit;
	CAN_FilterInitStructure.CAN_FilterIdHigh = GENID(DeviceID, CANMSG_DATA) << 5; // 0xE1
	CAN_FilterInitStructure.CAN_FilterIdLow = GENID(0, CANMSG_SYNC) << 5; // 0x80
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh = GENID(0, CANMSG_EMERGENCY) << 5; // 0x70
	CAN_FilterInitStructure.CAN_FilterMaskIdLow = GENID(0, CANMSG_HEARTBEAT) << 5; // 0xD0
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment = CAN_FilterFIFO0;
	CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
	CAN_FilterInit(&CAN_FilterInitStructure);

	/* Configure CAN RX0 interrupt. Priority 13 */
	CAN_ITConfig(CANLIB_CAN, CAN_IT_FMP0, ENABLE);
	NVIC_InitStructure.NVIC_IRQChannel = CAN1_RX0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY + 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}


CANError IAP::ReceiveFromHost(CanRxMsg *RxMessage) {

	if (GETMSG(RxMessage->StdId) == CANMSG_HTD) {
		/* Первый пакет, инициализируем переменную количества байт и пакетов */
		m_iBytesToReceive = RxMessage->Data[0];
		m_iBytesToReceive |= RxMessage->Data[1];

		m_iMessagesToReceive = m_iBytesToReceive/7;
		if (m_iBytesToReceive % 7 > 0)
			m_iMessagesToReceive++;
		m_iMaxMessageIndex = m_iMessagesToReceive;

		memcpy(&m_iCrcIn, &RxMessage->Data[2], 4);
		return CANError::CAN_OK;
	}

	if (GETMSG(RxMessage->StdId) == CANMSG_DATA) {
		/* Тут основные данные */
		uint32_t index = RxMessage->Data[0];
		if (index >= m_iMaxMessageIndex) {
			uart_printf("Index number is too large\n");
			return CANError::CAN_INDEX_TO_LARGE;
		}

		memcpy(&m_vRxBuffer[index*7], &RxMessage->Data[1], RxMessage->DLC - 1);
		m_iMessagesToReceive--;
	}

	if (m_iMessagesToReceive == 0) {
		/* Приняли все сообщения */
		pb_istream_t stream = pb_istream_from_buffer(m_vRxBuffer, m_iBytesToReceive);

		if (!pb_decode(&stream, HostToDevice_fields, &m_xHtD)) {
			uart_printf("Pb error\n");
			return CANError::CAN_ERROR_PB;
		}
		return CANError::CAN_PKT_READY;
	}
	return CANError::CAN_OK;
}

CANError IAP::ProcessPkt() {
CanTxMsg TxMsg;
int ret;

	if (m_xHtD.cmd1 == IAP_CMD1_ERASE) {
		uart_printf("Process Erase\n");
		TxMsg.StdId = FLASH_If_Erase(APPLICATION_ADDRESS) ? GENID(DeviceID, CANMSG_NACK) : GENID(DeviceID, CANMSG_ACK);
		if (GETMSG(TxMsg.StdId) == CANMSG_NACK)
			uart_printf("Erase Error\n");
	}

	if (m_xHtD.cmd1 == IAP_CMD1_WRITE) {
		CRC_ResetDR();
		uint32_t crc = CRC_CalcBlockCRC((uint32_t *)m_xHtD.data.bytes, m_xHtD.data.size/4);
		if (crc == m_xHtD.crc) {
			ret = FLASH_If_Write(&flashdst, (uint32_t *)m_xHtD.data.bytes, m_xHtD.data.size/4);
			if (ret == 0) {
				TxMsg.StdId = GENID(DeviceID, CANMSG_ACK);
				TxMsg.DLC = 0;
			} else {

				uart_printf("Block write error\n");
				TxMsg.StdId = GENID(DeviceID, CANMSG_NACK);
				TxMsg.DLC = 1;
				TxMsg.Data[0] = 0;
			}
		} else {
			/* CRC error */
			uart_printf("CRC Error\n");
			uart_printf("0x%08X != 0x%08X\n", crc, m_xHtD.crc);

			TxMsg.StdId = GENID(DeviceID, CANMSG_NACK);
			TxMsg.DLC = 1;
			TxMsg.Data[0] = 1;
		}
	}

	TxMsg.IDE = CAN_Id_Standard;
	TxMsg.RTR = CAN_RTR_Data;
	while (CAN_Transmit(CANLIB_CAN, &TxMsg) == CAN_TxStatus_NoMailBox);

	return CANError::CAN_OK;
}

void IAP::task() {
CanTxMsg TxMsg;
CanRxMsg RxMsg;

	TxMsg.StdId = GENID(DeviceID, CANMSG_HEARTBEAT);
	TxMsg.IDE = CAN_Id_Standard;
	TxMsg.RTR = CAN_RTR_Data;
	TxMsg.DLC = sizeof(IAP_VERSION);
	strncpy((char *)TxMsg.Data, IAP_VERSION, 8);
	while (CAN_Transmit(CANLIB_CAN, &TxMsg) == CAN_TxStatus_NoMailBox);

	/* Ждет ACK от компа */
	if (xQueueReceive(xCanQueue, &RxMsg, 3000) == pdTRUE) {
		if (GETMSG(RxMsg.StdId) == CANMSG_ACK) {
			uart_printf("Ack received\n");
		} else {
			uart_printf("Hui received\n");
		}
	}

	for (;;) {
		if (xQueueReceive(xCanQueue, &RxMsg, portMAX_DELAY) == pdTRUE) {
			switch (GETMSG(RxMsg.StdId)) {
			case CANMSG_SYNC:
				if ((RxMsg.DLC == 1) && (RxMsg.Data[0] == 0x5C)) {
					/* Сброс на основную программу */
					vTaskDelay(1000);
					RTC_WriteBackupRegister(RTC_BKP_DR0, 0x00000000);
					NVIC_SystemReset();
				}
				break;

			case CANMSG_HTD:
			case CANMSG_DATA:
				if (ReceiveFromHost(&RxMsg) == CANError::CAN_PKT_READY)
					ProcessPkt();
				break;

			default:
				break;
			}
		}
	}
}

/**
 * @function CAN1_RX0_IRQHandler
 * @breif Обработчик прерывания по получению сообщения CAN
 */
void CAN1_RX0_IRQHandler(void) {
CanRxMsg RxMessage;
BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	CAN_Receive(CANLIB_CAN, CAN_FIFO0, &RxMessage);

	assert_param(iap->xCanQueue != NULL);
	xQueueSendFromISR(iap->xCanQueue, &RxMessage, &xHigherPriorityTaskWoken);

	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

