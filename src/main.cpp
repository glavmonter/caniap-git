/**
  ******************************************************************************
  * @file    VGFThermo/main.c
  * @author  Vladimir Meshkov <glavmonter@gmail.com>
  * @version V1.0.0
  * @date    4-December-2012
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>
#include <rtc.h>
#include <string.h>
#include <stdbool.h>
#include "main.h"
#include "hardware.h"
#include "pb_decode.h"
#include "pb_encode.h"
#include "tlsf.h"
#include "flash_if.h"
#include "diag/Trace.h"
#include "iap.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
unsigned long ulRunTimeStatsClock;

/* Private function prototypes -----------------------------------------------*/

static void prvSetupHardware();
static void prvStartIwdt();
static uint32_t prvStartMain();

/* Private functions ---------------------------------------------------------*/
static char pool_ram[configTOTAL_HEAP_SIZE] __attribute__((aligned(8)));

/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
int main(void) {
	SystemCoreClockUpdate();
	prvSetupHardware();

	init_memory_pool(configTOTAL_HEAP_SIZE, pool_ram);

	uart_printf("\n\n");
	prvStartIwdt();

	/* Тут на самом деле должен быть POR */
	if (RCC_GetFlagStatus(RCC_FLAG_PORRST) == SET) {
		LED_RED(LED1);
		uart_printf("IAP. PowerOn Reset\n");

		RCC_ClearFlag();
		if (prvStartMain()) {
			/* Что-то с основной программой не так */
			StartIap();
		}
	}

	if (RCC_GetFlagStatus(RCC_FLAG_IWDGRST) == SET) {
		LED_RED(LED2);
		uart_printf("IAP. IWDG Reset\n");
		uart_printf("IAP. Main Program IWDG error\n");
		uart_printf("IAP. Starting IAP\n");
		RCC_ClearFlag();
		StartIap();
	}

	if (RCC_GetFlagStatus(RCC_FLAG_SFTRST) == SET) {
		LED_RED(LED3);
		uart_printf("IAP. Soft Reset\n");
		RCC_ClearFlag();
		if (RTC_ReadBackupRegister(RTC_BKP_DR0) == 0xAABBCCDD) {
			uart_printf("IAP from Main\n");
			RTC_WriteBackupRegister(RTC_BKP_DR0, 0x00000000);
			StartIap();
		} else {
			RTC_WriteBackupRegister(RTC_BKP_DR0, 0x00000000);
			if (prvStartMain()) {
				/* Что-то с основной программой не так */
				StartIap();
			}
		}
	}

	/* TODO Этот кусок возможно придется удалить */
	if (RCC_GetFlagStatus(RCC_FLAG_PINRST) == SET) {
		uart_printf("IAP. Pin Reset\n");
		RCC_ClearFlag();
		if (prvStartMain()) {
			/* Что-то с основной программой не так */
			StartIap();
		}
	}

	RCC_ClearFlag();
	RTC_WriteBackupRegister(RTC_BKP_DR0, 0x00000000);

#if configGENERATE_RUN_TIME_STATS == 1
	setupRunTimeStats();
#endif

	/* Infinite loop */
	while (1)
	{}
	return 0;
}



static void prvStartIwdt() {
	/* Enable the LSI oscillator ************************************************/
	RCC_LSICmd(ENABLE);
	/* Wait till LSI is ready */
	while (RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET){}

	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
	IWDG_SetPrescaler(IWDG_Prescaler_32); /* min - 1 ms, max - 4096 ms */
	IWDG_SetReload(0xFFF); // max
	IWDG_ReloadCounter();
	IWDG_Enable();
}

static uint32_t prvStartMain() {
	uart_printf("IAP: Starting Main\n");
	if (((*(__IO uint32_t *)APPLICATION_ADDRESS) & 0x2FFC0000) == 0x20000000) {
		uart_printf("IAP: Main stack Ok\n");

		__IO uint32_t JumpAddress = *(__IO uint32_t *)(APPLICATION_ADDRESS + 4);
		pFunction Jump_to_application = (pFunction)JumpAddress;

		__set_MSP(*(__IO uint32_t *)APPLICATION_ADDRESS);
		Jump_to_application();
	} else {
		uart_printf("IAP: Main stack Err\n");
	}
	return 1;
}

static void prvSetupHardware() {

	RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOB |
							RCC_AHB1Periph_GPIOC | RCC_AHB1Periph_GPIOD |
							RCC_AHB1Periph_GPIOE, ENABLE);
/*
GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = TIM1_CH0_PIN | TIM1_CH1_PIN | TIM1_CH2_PIN | TIM1_CH3_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(TIM1_CH0_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = TIM8_CH0_PIN | TIM8_CH1_PIN | TIM8_CH2_PIN | TIM8_CH3_PIN;
	GPIO_Init(TIM8_CH0_GPIO_PORT, &GPIO_InitStructure);

	TIM1_CH0_GPIO_PORT->BSRRH = TIM1_CH0_PIN | TIM1_CH1_PIN | TIM1_CH2_PIN | TIM1_CH3_PIN;
	TIM8_CH0_GPIO_PORT->BSRRH = TIM8_CH0_PIN | TIM8_CH1_PIN | TIM8_CH2_PIN | TIM8_CH3_PIN;

	GPIO_InitStructure.GPIO_Pin = LED1_PIN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(LED1_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = LED2_PIN;
	GPIO_Init(LED2_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = LED3_PIN;
	GPIO_Init(LED3_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = LED4_PIN;
	GPIO_Init(LED4_PORT, &GPIO_InitStructure);

	LED_GREEN(LED1);
	LED_GREEN(LED2);
	LED_GREEN(LED3);
	LED_GREEN(LED4);
*/
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_CRC, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);
	PWR_BackupAccessCmd(ENABLE);

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

	/* PA9 - USART1_Tx */
//	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1);
//	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
//	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
//	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
//	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
//	GPIO_Init(GPIOA, &GPIO_InitStructure);

//	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
//USART_InitTypeDef USART_InitStructure;
//	USART_InitStructure.USART_BaudRate = 115200;
//	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
//	USART_InitStructure.USART_StopBits = USART_StopBits_1;
//	USART_InitStructure.USART_Parity = USART_Parity_No;
//	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
//	USART_InitStructure.USART_Mode = USART_Mode_Tx;
//	USART_Init(USART1, &USART_InitStructure);
//	USART_Cmd(USART1, ENABLE);
}

/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
